package sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class Dictionary {

    private static Dictionary dictionary = null;
    private List<String> dictionaryWords = new ArrayList<>();

    public Dictionary(){
        BufferedReader input = null;
        dictionaryWords = new ArrayList<>();

        try {
            URL url = new URL("http://www.math.sjsu.edu/~foster/dictionary.txt");
            input = new BufferedReader(new InputStreamReader(url.openStream()));

            String line = "";
            while ((line = input.readLine()) != null) {
                dictionaryWords.add(line.trim());
            }

        }catch(MalformedURLException e){
            e.printStackTrace();
        } catch(IOException e){
            e.printStackTrace();
        } finally {
            try{
                input.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }

    }

    public static Dictionary getInstance(){
        if (dictionary == null){
            dictionary = new Dictionary();
        }
        return dictionary;
    }


    public List<String> getDictionaryWords() {
        return dictionaryWords;
    }

    public void setDictionaryWords(List<String> dictionaryWords) {
        this.dictionaryWords = dictionaryWords;
    }

}
