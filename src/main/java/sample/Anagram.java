package sample;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Path("/Test")
public class Anagram {

    List<String> dictionaryWords = new ArrayList<>();

    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String sayHello() {
        return "Hello Practice";
    }

    @GET
    @Path("/letters/{values}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getUsers(@PathParam("values") String values) {
        Dictionary dictionary = Dictionary.getInstance();
        dictionaryWords = dictionary.getDictionaryWords();

        String words = solveAnagram(values.toLowerCase());
        return "Input Letters: " + values + ", Words found in Dictionary: "+words;
    }


    public String solveAnagram(String letters){
        String words = "";
        int countLetters = letters.length();

        List<String> wordsLisTemp = new ArrayList<>();
        List<String> wordsListFinal = new ArrayList<>();
        List<String> wordList = new ArrayList<>();

        List<String> lettersList = new ArrayList<>();


        //Split the input value in array
        for (int i=0;i<letters.length();i++){
            lettersList.add(String.valueOf(letters.charAt(i)));
        }

        Collections.sort(lettersList);



        //Get list of words with exact number of characters
        for(String word : dictionaryWords){
            if (word.length() == countLetters){
                wordsLisTemp.add(word);
            }
        }


        //Compare words with the input value
        for (String word:wordsLisTemp){
            wordList = new ArrayList<>();

            for (int i=0;i<word.length();i++){
                wordList.add(String.valueOf(word.charAt(i)));
            }

            Collections.sort(wordList);

            if (lettersList.equals(wordList)){
                wordsListFinal.add(word);
            }
        }

        for (String s:wordsListFinal){
            words += s + ", ";
        }

        if (words.length()>0){
            words = words.substring(0,words.length()-2);
        }

        return words;
    }

}
